import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

final List<String> imgList = [
  'https://www.sk-ii.com/on/demandware.static/-/Sites-SKII-Library/default/dw95f6096f/images/exclusiveoffers/Desktop_2TO_Header13.png', //Banner1
  'https://www.sk-ii.com/on/demandware.static/-/Sites-NA_SKII/default/dw13d5a3fb/images/pdp-images/lifestyle_facial_treatment_essence-01.gif', //Banner2
  'https://www.sk-ii.com/on/demandware.static/-/Sites-SKII-Library/default/dw95f6096f/images/exclusiveoffers/Desktop_2TO_Header13.png', //Banner3
  'https://www.sk-ii.com/on/demandware.static/-/Sites-NA_SKII/default/dw13d5a3fb/images/pdp-images/lifestyle_facial_treatment_essence-01.gif' //Banner5
];

class CarouselDemo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: CarouselSlider(
          options: CarouselOptions(
            height: 500,
            aspectRatio: 16/9,
            viewportFraction: 0.8,
            initialPage: 0,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 3),
            scrollDirection: Axis.horizontal,
          ),
          items: imgList.map((item) => Container(
            width: 1300,
            child: Card(
              clipBehavior: Clip.antiAliasWithSaveLayer,
              shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
              child: Image.network(item, fit: BoxFit.contain, scale: 2)
            ),
          )).toList(),
        )
      ),
    );
  }
}