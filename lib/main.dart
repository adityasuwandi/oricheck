import 'dart:async';
import 'dart:io' show Platform;

import 'package:flutter/material.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'assets/carousel.dart' as carousel;

Map<int, Color> color = {
  50: Color.fromRGBO(4, 131, 184, .1),
  100: Color.fromRGBO(4, 131, 184, .2),
  200: Color.fromRGBO(4, 131, 184, .3),
  300: Color.fromRGBO(4, 131, 184, .4),
  400: Color.fromRGBO(4, 131, 184, .5),
  500: Color.fromRGBO(4, 131, 184, .6),
  600: Color.fromRGBO(4, 131, 184, .7),
  700: Color.fromRGBO(4, 131, 184, .8),
  800: Color.fromRGBO(4, 131, 184, .9),
  900: Color.fromRGBO(4, 131, 184, 1),
};

MaterialColor myColor = MaterialColor(0xFF880E4F, color);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: myColor,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Login Page'),
    );
  }
}

//Bottom Navbar Builder
class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key, this.userId}) : super(key: key);

  final String userId;

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;

    @override
  void initState() {
    super.initState();
    platform = new PlatformPage(userId: widget.userId);
    scanner = new _Scanner(userId: widget.userId);
  }

  static PlatformPage platform;
  static _Scanner scanner;
  
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  static List<Widget> _widgetOptions = <Widget>[
    platform,
    scanner,
    TransactionPage(items: List<String>.generate(10000, (i) => "Item $i"))
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.camera),
            title: Text('Scan'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.receipt),
            title: Text('History'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.redAccent[800],
        onTap: _onItemTapped,
      ),
    );
  }
}

//Login Page
class MyHomePage extends StatefulWidget {
  const MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

final emailController = TextEditingController();
final passwordController = TextEditingController();

class _MyHomePageState extends State<MyHomePage> {
  void checkLogin() {
    Firestore.instance
        .collection('users')
        .where("email", isEqualTo: emailController.text)
        .snapshots()
        .listen((data) => data.documents.forEach((doc) => {
              if (doc['password'] == passwordController.text) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyStatefulWidget(userId:doc.documentID)),
                )
              } else {
                showDialog(
                  context: context,
                  builder: (context) {
                    return AlertDialog(
                      // Retrieve the text the that user has entered by using the
                      // TextEditingController.
                      content: Text('Incorrect Password!'),
                    );
                  },
                )
              }
            }));
    }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              margin:
                  const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10),
              height: 75,
              child: Image.network(
                  'https://upload.wikimedia.org/wikipedia/commons/7/7b/Logo_SK-II.png'),
            ),
            Container(
                margin:
                    const EdgeInsets.only(left: 50.0, right: 50.0, bottom: 10),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(10),
                      child: TextField(
                        controller: emailController,
                        obscureText: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Mail',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      child: TextField(
                        controller: passwordController,
                        obscureText: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Password',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      height: 50,
                      width: 200,
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        color: myColor,
                        onPressed: checkLogin,
                        child: const Text('Log in',
                            style: TextStyle(fontSize: 20, color: Colors.white, fontWeight: FontWeight.bold)),
                      ),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}

//User Platform Page

class PlatformPage extends StatefulWidget {
  const PlatformPage({Key key, this.userId}) : super(key: key);
  final String userId;


  @override
  _PlatformPageState createState() => _PlatformPageState();
}

class _PlatformPageState extends State<PlatformPage> {
  
  String userName = "";
  int userPoints = 0;

  void getUserInfo(String userId){
    Firestore.instance
        .collection('users')
        .document(userId)
        .get()
        .then((DocumentSnapshot ds) {
          setState(() {
          userName = ds.data['name'];
          userPoints = ds.data['points'];
    });
          
    });
  }
  @override
  Widget build(BuildContext context) {
    getUserInfo(widget.userId);
    return Scaffold(
        body: ListView(children: <Widget>[
      Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Colors.red, myColor])),
        child: Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(bottom: 10),
            ),
            Center(
              child: Text(userName,
                  style: TextStyle(fontSize: 25, color: Colors.white)),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 30),
            ),
            Center(
              child: Text(userPoints.toString(),
                  style: TextStyle(fontSize: 90, color: Colors.white)),
            ),
            Center(
              child: Text('SK Points',
                  style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.bold)),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 50),
            )
          ],
        ),
      ),
      Container(
              margin: const EdgeInsets.only(bottom: 5),
            ),
      Card(
        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const ListTile(
              leading: Icon(Icons.notifications),
              title: Text('Do you want to receive notifications?'),
              subtitle: Text('Get the latest news and updates of our product'),
            ),
            ButtonBar(
              children: <Widget>[
                FlatButton(
                  child: const Text('Yes'),
                  onPressed: () {/* ... */},
                ),
                FlatButton(
                  child: const Text('No', style: TextStyle(color: Colors.grey)),
                  onPressed: () {/* ... */},
                ),
              ],
            ),
          ],
        ),
      ),
      Container(
              height: 200,
              child: new carousel.CarouselDemo(),
            ),
      
    ]));
  }
}

//QR Scanner Function
class _Scanner extends StatefulWidget {
  const _Scanner({Key key, this.userId}) : super(key: key);
  final String userId;

  @override
  _ScannerState createState() => _ScannerState();
}

class _ScannerState extends State<_Scanner> {
  ScanResult scanResult;


  final _flashOnController = TextEditingController(text: "Flash on");
  final _flashOffController = TextEditingController(text: "Flash off");
  final _cancelController = TextEditingController(text: "Cancel");

  var _isvalid;
  var _itemName;
  var _addedPoints;
  var _aspectTolerance = 0.00;
  var _numberOfCameras = 0;
  var _selectedCamera = -1;
  var _useAutoFocus = true;
  var _autoEnableFlash = false;

  static final _possibleFormats = BarcodeFormat.values.toList()
    ..removeWhere((e) => e == BarcodeFormat.unknown);

  List<BarcodeFormat> selectedFormats = [..._possibleFormats];

  @override
  // ignore: type_annotate_public_apis
  initState() {
    super.initState();

    Future.delayed(Duration.zero, () async {
      _numberOfCameras = await BarcodeScanner.numberOfCameras;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    var contentList = <Widget>[
      if (scanResult != null && scanResult.type?.toString() != "Cancelled")
        if(_isvalid != true)
          Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(
                  top: 100, left: 20.0, right: 20.0, bottom: 10),
              height: 200,
              child: Image.network(
                  'https://cdn.iconscout.com/icon/free/png-256/false-delete-remove-cross-wrong-36-32770.png'),
            ),
            Container(
                margin:
                    const EdgeInsets.only(left: 50.0, right: 50.0, bottom: 10),
                child: Column(
                  children: <Widget>[
                    Center(
                      child: Text("Sorry, we can't recognize this code",
                          style: TextStyle(fontSize: 15, color: myColor, fontWeight: FontWeight.bold)),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        color: myColor,
                        onPressed: scan,
                        child: const Text('Scan Again',
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                      ),
                    )
                  ],
                ))
          ],
        )
        else
          Card(
          child: Column(
            children: <Widget>[
            Container(
              margin: const EdgeInsets.only(
                  top: 100, left: 20.0, right: 20.0, bottom: 10),
              height: 300,
              child: Image.network(
                  'https://cdn.dribbble.com/users/6257/screenshots/3833147/coin.gif'),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 10),
            ),
            Center(
              child: Text('Succesfully redeemed ' + _addedPoints.toString() +' points from',
                  style: TextStyle(fontSize: 14, color: myColor, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
            ),
            Center(
              child: Text(_itemName,
                  style: TextStyle(fontSize: 10, color: myColor), textAlign: TextAlign.center),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 50),
            )
            ],
          ))
      else if (scanResult != null &&
              scanResult.type?.toString() == "Cancelled" ||
          scanResult == null)
        Column(
          children: <Widget>[
            Container(
              margin: const EdgeInsets.only(
                  top: 100, left: 20.0, right: 20.0, bottom: 10),
              height: 300,
              child: Image.network(
                  'https://cdn.dribbble.com/users/1046956/screenshots/4468756/qrscananimation.gif'),
            ),
            Container(
                margin:
                    const EdgeInsets.only(left: 50.0, right: 50.0, bottom: 10),
                child: Column(
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.all(10),
                      child: RaisedButton(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                        ),
                        color: myColor,
                        onPressed: scan,
                        child: const Text('Scan Now',
                            style:
                                TextStyle(fontSize: 20, color: Colors.white)),
                      ),
                    )
                  ],
                ))
          ],
        )
    ];

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.white,
        body: ListView(
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          children: contentList,
        ),
      ),
    );
  }

  void showSuccessDialog() {
    final DocumentReference userRef = Firestore.instance.document('users/'+widget.userId);
    Firestore.instance
        .collection('products')
        .where("skuid", isEqualTo: scanResult.rawContent)
        .where("redeemed", isEqualTo: false)
        .snapshots()
        .listen((data) => {
          if (data.documents.isEmpty == true){
            setState(() => _isvalid = false)
          } else {
            setState(() => _isvalid = true),
            Firestore.instance.runTransaction((Transaction tx) async {
              DocumentSnapshot userSnapshot = await tx.get(userRef);
              if (userSnapshot.exists) {
                await tx.update(userRef, <String, dynamic>{'points': userSnapshot.data['points'] + data.documents.first['points']});
                await Firestore.instance.collection('users').document(widget.userId).collection('pointsHistory').document().setData({'product':data.documents.first['name'], 'points': data.documents.first['points'], 'previous': userSnapshot.data['points'], 'now': userSnapshot.data['points'] + data.documents.first['points'], 'createdDate': DateTime.now()});
                await Firestore.instance.collection('products').document(data.documents.first.documentID).updateData({'owner':widget.userId, 'redeemed': true});
              }
            },
            ),
            setState(() => _itemName = data.documents.first['name']),
            setState(() => _addedPoints = data.documents.first['points'])
          }
        });
  }

  Future scan() async {
    try {
      var options = ScanOptions(
        strings: {
          "cancel": _cancelController.text,
          "flash_on": _flashOnController.text,
          "flash_off": _flashOffController.text,
        },
        restrictFormat: selectedFormats,
        useCamera: _selectedCamera,
        autoEnableFlash: _autoEnableFlash,
        android: AndroidOptions(
          aspectTolerance: _aspectTolerance,
          useAutoFocus: _useAutoFocus,
        ),
      );

      var result = await BarcodeScanner.scan(options: options);
      setState(() => scanResult = result);
      showSuccessDialog();
    } on PlatformException catch (e) {
      var result = ScanResult(
        type: ResultType.Error,
        format: BarcodeFormat.unknown,
      );

      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          result.rawContent = 'The user did not grant the camera permission!';
        });
      } else {
        result.rawContent = 'Unknown error: $e';
      }
      setState(() {
        scanResult = result;
      });
    }
  }
}

// Transaction History Page

class TransactionPage extends StatelessWidget {
  TransactionPage({Key key, this.items, this.userId}) : super(key: key);

  final List<String> items;
  final String userId;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('users').document('J7lxTlviGApSgqLB1q70').collection('pointsHistory').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError)
          return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting: return new Text('Loading...');
          default:
            return new ListView(
              children: snapshot.data.documents.map((DocumentSnapshot document) {
                return new ListTile(
                  title: new Text("Redeemed " + document['points'].toString() + ' points'),
                  subtitle: new Text(document['product'].toString()),
                );
              }).toList(),
            );
        }
      },
    );
  }
}
